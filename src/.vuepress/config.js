module.exports = {
    title: `Afterhours Note by Czenhe`,
    locales: {
        '/': {
        lang: 'ja-JP',
        description: '日常の中で、食べて、遊んで、観て、聴いて、感じて。経験を記録する。'
        },
        '/ko/': {
        lang: 'ko-KR',
        description: '일상속에서 먹고 놀고 보고 듣고 느끼는 경험들을 하나하나 소중히 기록하는 공간'
        }
    },
    head: [
        ['link', { rel: 'icon', type: 'image/png', href: '/favicon.png' }],
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Reenie+Beanie|Sawarabi+Gothic'}]
    ],
    plugins: {
        '@vuepress/google-analytics': {
            ga: 'UA-34600242-6'
        }
    },
    themeConfig: {
        logo: '/assets/img/logo.png',
        // navbar: false,
        // displayAllHeaders: true,
        // activeHeaderLinks: false,
        search: false,
        nav: [
            { text: 'Note', link: '/note/' },
            {
                text: 'Other',
                items: [
                  { text: 'Instagram', link: 'https://www.instagram.com/czenhe.xyz/' },
                  { text: 'facebook', link: 'https://www.facebook.com/czeunhe' },
                  { text: 'CodeLog', link: 'https://codelog.dev' }
                ]
            }
        ],
        sidebar: {
            '/note/': [
                '',
                {
                  title: 'Daily Log',
                  collapsable: false,
                  children: [
                    '/note/daily/'
                  ]
                },
                {
                  title: 'My Map',
                  collapsable: true,
                  children: [ ]
                }
            ],
            
            // fallback
            '/': false
        }
    }    
}