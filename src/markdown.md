# markdown sample

# H1

## H2

### H3

*Italic*

`tag`

```
code
```

> block

<img :src="$withBase('/foo.png')" alt="foo">

### link
[Home](/) <!-- Sends the user to the root README.md --><br>
[foo](/foo/) <!-- Sends the user to index.html of directory foo --><br>
[foo heading anchor](/foo/#heading) <!-- Anchors user to a heading in the foo README file --><br>
[foo - one](/foo/one.html) <!-- You can append .html --><br>
[foo - two](/foo/two.md) <!-- Or you can append .md -->

Front Matter
YAML front matter is supported out of the box when placed at the very beginning of your markdown file:

```
---
title: Blogging Like a Hacker
lang: en-US
---
```
The data will be available to the rest of the page, plus all custom and theming components as $page.

title and lang will be automatically set on the current page. In addition you can specify extra meta tags to be injected:
```
---
meta:
  - name: description
    content: hello
  - name: keywords
    content: super duper SEO
---
```

# Alternative Front Matter Formats
In addition, VuePress also supports JSON or TOML front matter.

JSON front matter needs to start and end in curly braces:
```
---
{
  "title": "Blogging Like a Hacker",
  "lang": "en-US"
}
---
```
TOML front matter needs to be explicitly marked as TOML:
```
---toml
title = "Blogging Like a Hacker"
lang = "en-US"
---
```

# GitHub-Style Tables
Input
```
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
```
Output

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

# Emoji 🎉
Input
```
:tada: :100:
```
Output
:tada: :100:

A list of all emojis available can be found here.

# Table of Contents
Input
```
[[toc]]
```
Output
[[toc]]

Rendering of TOC can be configured using the markdown.toc option.

# Custom Containers
Input
```
::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger
This is a dangerous warning
:::
```
Output

::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger
This is a dangerous warning
:::

You can also customize the title of the block:

```
::: danger STOP
Danger zone, do not proceed
:::
```
::: danger STOP
Danger zone, do not proceed
:::

# Line Highlighting in Code Blocks

``` js{3-5}
export default {
  data () {
    return {
      msg: 'Highlighted!'
    }
  }
}
```


